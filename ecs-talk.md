theme: Fira
autoscale: true 

# [fit] James McMahon

http://jamesmcmahon.net/

# [fit] I, Falling Robot

http://ifallingrobot.com/

(iOS and Android)

---

![fit](https://www.youtube.com/watch?v=UhhmcT5yHCw)

---

# I, Falling Robot

* Simple mechanics but some advanced features 
* Beat detection (offline)
* Auto tuned difficulty
* Auto calibrated controls
* Two game modes
* Leaderboards
* Achievements

---

# I, Falling Robot (cont.)

* ~14k Total LOC (25k with tests and generated code)
* 3431 in Unity scripts
* 10446 in ECS code
* 4922 in generated code
* 6762 in tests
* Uses an Entity Component System architecture

---

# [fit] Entity
# [fit] Component
# [fit] System

(ECS for short)

--- 

# [fit] ![](images/entitas-e.png) Entitas

For Unity

https://github.com/sschmid/Entitas-CSharp

--- 

# [fit] Why ECS?

---

# Prototyping "Falling"

* Started prototyping simple games
* Quick and dirty code, getting it done ASAP
* This prototype eventually became I, Falling Robot

![right fit](images/0-falling.gif)

---

# Prototyping "Reverse PacMan"

* Initial prototypes fit Unity well
* This one uses NavMesh agents and very little code
* Not doing much iteration at this point

![left fit](images/1-pacman.gif)

---

# Prototyping "Turn Based Resident Evil"

* Started getting into more complex prototypes
* More iterations to add features
* Iterations were becoming costly

![right](images/3-return-1.gif)

---

# Prototyping pain points

* Needed to be able to write clean and reusable code *quickly*
* Needed to be able add and remove features *quickly*
* Needed an architecture that I could reapply to different prototypes
* Needed something that was going to fit in well with Unity

![left](images/3-return-2.gif)

---

# Enter Entitas (and ECS)

## The Promise

* Clean and reusable code
* Easy iteration
* Scalable architecture
* Solutions to Unity pain points

---

# Unity pain points
### (According to Entitas creators)

* Testability
* Code sharing (client and server)
* Co-dependent logic (encapsulation)
* Querying
* Deleting code

---

# So what is ECS?

---

# Entity

* Rough analogue of Unity's game objects
* Just a container for components
* An entity without components is useless

---

# Component

* Analogue of MonoBehavior with one important difference 
* Just data
* No methods

---

# Sample Components

Position on a grid

```cs
public class PositionComponent : Entitas.IComponent
{
    public float x;
    public float y;
}
```

Health for Player, Monsters, NPCs, etc

```cs
public class HealthComponent : Entitas.IComponent
{
    public int hitPoints;
}
```

---

# Components can also be flags

Empty Components can still mark an Entity

Mark as invincible

```cs
public class InvincibleComponent : Entitas.IComponent
{
}
```

Mark an entity to be removed

```cs
public class DestroyComponent : Entitas.IComponent
{
}
```

---

# Pool

* Entitas specific
* Pool contains Entities
* Can add and remove Entities from Pool

---

# Group

* Groups are subsets of a Pool
* Queries against the Pool to find Entities
* If Pools are the database, Groups are the queries
* Unlike a database not looking up by state but by the *presence* of Components

---

# Recap

```
+------------------+
|       Pool       |
|------------------|
|    e       e     |      +-----------+
|        e     e---|----> |  Entity   |
|  e        e      |      |-----------|
|     e  e       e |      | Component |
| e            e   |      |           |      +-----------+
|    e     e       |      | Component-|----> | Component |
|  e    e     e    |      |           |      |-----------|
|    e      e    e |      | Component |      |   Data    |
+------------------+      +-----------+      +-----------+
  |
  |
  |     +-------------+
  |     |      e      | Groups
  |     |   e     e   |
  +---> |        +------------+
        |     e  |    |       |
        |  e     | e  |  e    |
        +--------|----+    e  |
                 |     e      |
                 |  e     e   |
                 +------------+
```

---

# [fit] This is all state

---

![](images/allstate-ad.jpg)

---


> "It is better to have 100 functions operate on one data structure than 10 functions on 10 data structures."
-- Alan Perlis

---

# System

* Where the code lives
* No state
* Reads global state in Pool
* Changes the global state (if needed)

---

> “Games are interactive simulations”
-- Maxim Zaks (Entitas Author)

---

# Sample Systems

Falling System

```cs
void Execute()
{
    foreach (var entity in fallingGroup.GetEntities())
    {
        var pos = entity.position;
        var speed = entity.falling.speed;
        entity.ReplacePosition(pos.x, pos.y + speed);
    }
}
```

---

# Sample Systems

Edge system, destroy Entities as the reach the end of game space

```cs
void Execute()
{
    var edgePos = pool.edgeEntity.position;
    foreach (var entity in positionGroup.GetEntities())
    {
        if (entity.position.y >= edgePos.y)
        {
            entity.isDestroy = true;
        }
    }
}
```

---

# Reactive Systems

* Execute systems fire every frame
* Reactive systems react to changes in the Pool
* Fire only on the frames that they are needed

---

# Sample Reactive System

Exit system, waits for the exit to be reach, then goes to next level

```cs
TriggerOnEvent IReactiveSystem.trigger
{
    get
    {
        return Matcher.AllOf(Matcher.Player, Matcher.Position).OnEntityAdded();
    }
}

void IReactiveExecuteSystem.Execute(List<Entity> entities)
{
    var playerPos = pool.playerEntity.position;
    foreach (var exit in exitGroup.GetEntities())
    {
        if (playerPos.Equals(exit.position))
        {
            int currentLevel = pool.level.level;
            pool.ReplaceLevel(currentLevel + 1);
            return;
        }
    }
}
```

---

# Initialization System

Pretty simple, just initialized at the start of the game

CreatePlayerSystem

```cs
void Initialize()
{
    int lives = 3;
    pool.CreateEntity()
        .AddResource(ViewResource.Player)
        .AddPosition(0, Constants.PlayerPositionY)
        .AddHealth(lives, lives)
        .IsMoveable(true)
        .IsPlayer(true);
}
```

---

# [fit] In Practice

![right](images/roguelike-1.gif)

---

# Original Code

Player.cs is responsible for

* Reading input
* Moving player
* Damaging walls
* Picking up food
* Updating food text
* Check for game over
* Etc

---

# ECS Code

Multiple system each with a single job

* MoveSystem
* DamageSpriteSystem
* FoodSystem
* GameOverSystem
* Etc

---

# ECS Improvements

Because code is encapsulated, it's easier to read, understand and extend. Since they handle one thing Systems tend to be small and to the point.

FoodSystem.cs

```cs
void IReactiveExecuteSystem.Execute(List<Entity> entities)
{
    var playerPosition = pool.playerEntity.position;
    ICollection<Entity> entitiesAtPos;
    pool.IsGameBoardPositionOpen(playerPosition, out entitiesAtPos);

    Entity foodEntity;
    if (entitiesAtPos != null &&
        entitiesAtPos.ContainsComponent(ComponentIds.Food, out foodEntity))
    {
        pool.AddToFoodBag(foodEntity.food.points);
        pool.PlayAudio(foodEntity.audioPickupSource);
        pool.DestroyEntity(foodEntity);
    }
}
```

---

# Testing

Tests for systems usually consist of 3 parts.

1. Setup initial state
2. Run system
3. Check post run state

```cs
[Test]
public void RemovesTouchInput()
{
    pool.isTouchInput = true;

    systems.Execute();

    Assert.IsTrue(pool.touchInputEntity.isDestroy);
}
```

---

# Testing (cont.)

```cs
[Test]
public void RemoveSingleBreakableEntity()
{
    pool.isTouchInput = true;
    pool.CreateEntity()
        .AddPosition(0, 0)
        .IsBreakable(true)
        .IsObstacle(true);

    systems.Execute();

    var entites = pool.GetEntities(Matcher.AllOf(
                          Matcher.Position,
                          Matcher.Obstacle,
                          Matcher.Breakable));
    Assert.IsTrue(entites[0].isDestroy);
}

[Test]
public void PlaysSound()
{
    pool.isTouchInput = true;
    pool.CreateEntity()
        .AddPosition(0, 0)
        .IsBreakable(true)
        .IsObstacle(true);

    systems.Execute();

    var effects = pool.GetGroup(Matcher.SoundEffect).GetEntities();
    Assert.AreEqual(1, effects.Length);
    Assert.AreEqual(SoundEffect.Birds, effects[0].soundEffect.effect);
}
```

---

# [fit] "Rogue Like" Example Code

https://github.com/JamesMcMahon/entitas-2d-roguelike

# [fit] Entitas

https://github.com/sschmid/Entitas-CSharp
